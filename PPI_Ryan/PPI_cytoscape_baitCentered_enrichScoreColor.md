[TOC]

# Re-arrange PPI in Cytoscape
## move the bait central & map enrich score as node color 

Prepare a interaction relationship file for importing to Cytoscape, here SIF(Simple Interaction Format), .sif file only has 3 columns: interactor A, interaction type, interactior B

|interactor A|interaction type|interactior B
|---|---|---|
|bait|PPI| A|
|bait| PPI| B|
|bait| PPI| C|
|bait| PPI| D|
|bait| PPI| E|
|bait| PPI| F|
|bait| PPI| G|
|bait| PPI| H|
---

Prepare a score file which denote enrichment of each protein except �bait�

\*feature file: mappped_column, feature 1, feature 2,...

\*here only has enrichment score as annotated feature 

|protein|	enrich_score|
|---|---|
|A	|10|
|B	|9|
|C	|8|
|D	|7|
|E	|6|
|F	|5|
|G	|4|
***

### import .sif
![](./import_sif.jpeg)

### imported default network
![](./imported_sif.png)

### choose desired layout, here circular
>Choose Circular style for display.
>Generally all the molecules are arranged as a circle. So you need to manual select the �bait� protein and move it to the center.
>Here because only bait interact with other proteins(no prey-prey interaction), the network by default arranged as we desired.
![](./choose_layout_circule.png)

### Import our enrich score(feature) file
![](./import_feature.png)
![](./set_feature_map_column.jpeg)

### Map column to node feature

1. Select style(change network style)
2. Choose node(we want map color to node)
3. Select Fill Color, Choose column: enrich_score, Set mapping type: Continuous mapping
Default color:
lowest value -> black
Highest value -> white
4. Double click the color area to change the up/bottom mapping color
![](./map_feature.jpeg)

### choose color for lowest value
![](./set_low_value_color.jpeg)

### choose color for highest value
![](./set_high_value_color.jpeg)

### finally we are here: Bait in the network center, Enrichment score mapped as color
![](./finally.png)
